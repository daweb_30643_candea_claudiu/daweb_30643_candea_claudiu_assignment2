import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Destination } from 'src/app/interfaces/Destination';
import { User } from 'src/app/interfaces/User';
import { DestinationService } from 'src/app/services/destination.service';
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { EditFormComponent } from '../edit-form/edit-form.component';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.css']
})
export class DestinationsComponent {
  startDate: Date = new Date();
  endDate: Date = new Date();
  destinations:Destination[] = []
  offers:Destination[] = []
  constructor(private route: ActivatedRoute,public sessionService:SessionStorageService,private destiantionService:DestinationService,
    private router:Router,public dialog:MatDialog) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{
      const location = params['location']
      if(location=='all'){
        this.destiantionService.getAll().subscribe(res =>{
          this.destinations = res.filter(dest => dest.offer==0);
          this.offers = res.filter(dest => dest.offer!=0);
        }
        )
      }
      else {
        this.destiantionService.getFilteredDestinations(location).subscribe(res => {
          this.destinations = res.filter(dest => dest.offer==0);
          this.offers = res.filter(dest => dest.offer!=0);
          })
      }
    })
    this.route.fragment.subscribe(fragment => {
      console.log('Received fragment:', fragment);
      if (fragment) {
        const element = document.getElementById(fragment);
        console.log('Element:', element);
        if (element) {
          element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
        }
      }
    });
  }

  getRole():String{
    let user:User = this.sessionService.getItem('currUser');
    if(user==null){
      return "";
    }
    else{
      return user.role;
    }
  }

  navigateToCreate(){
    this.router.navigate(['create']);
  }

  deleteDestination(id:number){
    this.destiantionService.delete(id);
    console.log("in delete")
    this.refresh();
  }

  refresh(){
    this.destiantionService.getAll().subscribe(res =>{
      this.destinations = res.filter(dest => dest.offer==0);
      this.offers = res.filter(dest => dest.offer!=0);
  }
  )
  }

  editDestination(destination:Destination){
    const dialogRef = this.dialog.open(EditFormComponent,{
      data : {id: destination.id,title:destination.title,description:destination.description,location:destination.location,
        pricePerNight:destination.pricePerNight,freeRooms:destination.freeRooms,offer:destination.offer,imageUrl:destination.imageUrl},
    })

    dialogRef.afterClosed().subscribe(res => {
      this.refresh();
    });
    this.refresh();
  }



}
